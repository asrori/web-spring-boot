/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webspringbot.service;

import com.webspringbot.model.Mahasiswa;
import java.util.List;

/**
 *
 * @author USER
 */
public interface MahasiswaService 
{
    List<Mahasiswa> listMahasiswa();
    Mahasiswa saveOrUpdate(Mahasiswa m);
    Mahasiswa getIdMahasiswa(Integer id);
    void hapus(Integer id);
}
